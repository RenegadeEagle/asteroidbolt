package me.renegadeeagle.co.projects.asteroidrunner;

import me.renegadeeagle.co.projects.asteroidrunner.background.Background;
import me.renegadeeagle.co.projects.asteroidrunner.entity.Entity;
import me.renegadeeagle.co.projects.asteroidrunner.entity.EntityAsteroid;
import me.renegadeeagle.co.projects.asteroidrunner.entity.EntityHandler;
import me.renegadeeagle.co.projects.asteroidrunner.image.ImageLoader;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Ryan on 9/20/2014.
 */
public class AsteroidRunner {

    public static JFrame frame = new JFrame();

    private static String VERSION = "v1.0";

    private static GamePanel gamePanel = new GamePanel();

    private static int WIDTH = 1000;

    private static int HEIGHT = 600;

    private static Background bg1, bg2;

    public static List<Entity> entities = new ArrayList<>();

    private static EntityHandler entityHandler = new EntityHandler();
    private static Random random = new Random();
    public static void main(String[] args){
        frame.setSize(WIDTH, HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Asteroid Runner " + VERSION);
        frame.setVisible(true);
        frame.add(gamePanel);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        bg1 = new Background(0);
        bg2 = new Background(1000);

        ImageLoader.loadImages();

        Random random = new Random();
        int speed = random.nextInt(10);
        System.out.println(speed);
        EntityAsteroid asteroid = new EntityAsteroid(950, 100, speed);
        entities.add(asteroid);

        beginHeartbeat();
    }

    public static void tick(){
        gamePanel.repaint();
        bg1.update();
        bg2.update();
        int chance = random.nextInt(100);
        if(chance == 30){
            EntityAsteroid asteroid = entityHandler.spawnRandomAsteroid();
            entities.add(asteroid);
            System.out.println("asteroid spanwed");
        }
        for(Entity entity : entities){
            entity.tick();
        }
    }

    private static Thread thread = null;
    private static boolean running = false;
    public static void beginHeartbeat(){
        running = true;
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (running) {
                    tick();
                    try {
                        Thread.sleep(27);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.run();

    }
    public static void paint(Graphics g, int width, int height){
        g.fillRect(0, 0, width, height);

        g.drawImage(ImageLoader.BACKGROUND, bg1.getX(), 0, gamePanel);
        g.drawImage(ImageLoader.BACKGROUND, bg2.getX(), 0, gamePanel);
        for(Entity e: entities){
            if(e instanceof EntityAsteroid){
                System.out.println(((EntityAsteroid) e).getSpeed());
                g.drawImage(((EntityAsteroid) e).getCurrentImage(), (int) e.getPosition().getX(), (int) e.getPosition().getY(), gamePanel);
            }
        }
    }
}
