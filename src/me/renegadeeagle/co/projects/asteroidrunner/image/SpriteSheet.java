package me.renegadeeagle.co.projects.asteroidrunner.image;

import java.awt.Image;
import java.awt.image.BufferedImage;

public class SpriteSheet {
	private BufferedImage image;
	private int rows;
	private int cols;
	private int width;
	private int height;
	private BufferedImage[] frames;

	public SpriteSheet(BufferedImage image, int rows, int cols, int width, int height){
		this.image = image;
		this.rows = rows;
		this.cols = cols;
		this.width = width;
		this.height = height;
		frames = new BufferedImage[rows * cols];
		System.out.println(rows * cols);
		for(int x = 0; x < rows; x++){
			for(int y = 0; y < cols; y++){
				int index = (x * cols) + y;
				frames[index] = image.getSubimage(y * width, x * height, width, height);

			}
		}
	}
	public BufferedImage[] getImages(){
		return frames;
	}
	/**
	 * @return the image
	 */
	public BufferedImage getImage() {
		return image;
	}
	/**
	 * @return the rows
	 */
	public int getRows() {
		return rows;
	}
	/**
	 * @return the cols
	 */
	public int getCols() {
		return cols;
	}
	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}
	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}
	/**
	 * @return the frames
	 */
	public BufferedImage[] getFrames() {
		return frames;
	}
	public Image getImage(int index){
        return frames[index - 1];
    }

}
