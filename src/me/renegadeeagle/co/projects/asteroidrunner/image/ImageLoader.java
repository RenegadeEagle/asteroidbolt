package me.renegadeeagle.co.projects.asteroidrunner.image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by Ryan on 9/20/2014.
 */
public class ImageLoader {

    public static BufferedImage BACKGROUND = null;
    public static BufferedImage EXPLOSIONSPRITE = null;
    public static BufferedImage ASTEROID = null;
    public static void loadImages(){
        try{
            BACKGROUND = ImageIO.read(new File("res/Game_Background.png"));
            EXPLOSIONSPRITE = ImageIO.read(new File("res/Explosion.png"));
            ASTEROID = ImageIO.read(new File("res/asteroid.png"));
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
