package me.renegadeeagle.co.projects.asteroidrunner.background;

/**
 * Created by Ryan on 9/20/2014.
 */
public class Background {
    private int x;
    private int speed;
    public Background(int x){
        this.x=x;
        speed-=3;
    }
    public void update() {
        x += speed;
        if (x <= -1000){
            x += 2000;
        }
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
