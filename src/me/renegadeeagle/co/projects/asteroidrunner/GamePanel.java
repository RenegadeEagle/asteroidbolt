package me.renegadeeagle.co.projects.asteroidrunner;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Ryan on 9/20/2014.
 */
public class GamePanel extends JPanel {
    @Override
    public void paint(Graphics g){
        AsteroidRunner.paint(g, this.getWidth(), this.getHeight());
    }
}
