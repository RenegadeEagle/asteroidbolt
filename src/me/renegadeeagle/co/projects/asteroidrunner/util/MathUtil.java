package me.renegadeeagle.co.projects.asteroidrunner.util;

import java.util.Random;

/**
 * Created by Ryan on 9/21/2014.
 */
public class MathUtil {
    public static int betweenTwo(int min, int max){
        Random random = new Random();
        int between = random.nextInt(max-min)+min;
        return between;
    }
}
