package me.renegadeeagle.co.projects.asteroidrunner.entity;

import me.renegadeeagle.co.projects.asteroidrunner.image.Animation;
import me.renegadeeagle.co.projects.asteroidrunner.image.SpriteSheet;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Ryan on 9/20/2014.
 */
public abstract class Entity {

    protected String displayname;
    protected SpriteSheet sheet;
    protected EntityType type;
    protected Point position;


    protected Entity(String displayname, EntityType type, SpriteSheet sheet, Point position) {
        this.displayname = displayname;
        this.type = type;
        this.sheet = sheet;
        this.position = position;
    }

    public abstract void tick();

    public abstract Image getImage();

    public boolean isOnScreen(){
        if(position.getX() > 800 || position.getX() < 0 || position.getY() > 600 ){
           return false;
        }
        return false;
    }
    public void setPosition(Point p){
        position = p;
    }
    public void setPosition(int x, int y){
        position = new Point(x, y);
      }

    public String getDisplayname() {
        return displayname;
    }

    public SpriteSheet getSheet() {
        return sheet;
    }

    public EntityType getType() {
        return type;
    }

    public Point getPosition() {
        return position;
    }

}
