package me.renegadeeagle.co.projects.asteroidrunner.entity;

import me.renegadeeagle.co.projects.asteroidrunner.util.MathUtil;

import java.util.Random;

/**
 * Created by Ryan on 9/21/2014.
 */
public class EntityHandler {

    private static Random random = new Random();
    public EntityAsteroid spawnRandomAsteroid(){
        int y = random.nextInt(600);
        int speed = MathUtil.betweenTwo(4, 10);
        EntityAsteroid asteroid = new EntityAsteroid(900, y, speed);

        return asteroid;
    }

}
