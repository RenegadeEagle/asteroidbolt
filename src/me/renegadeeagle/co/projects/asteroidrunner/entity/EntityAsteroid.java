package me.renegadeeagle.co.projects.asteroidrunner.entity;

import me.renegadeeagle.co.projects.asteroidrunner.image.ImageLoader;
import me.renegadeeagle.co.projects.asteroidrunner.image.SpriteSheet;

import java.awt.*;

/**
 * Created by Ryan on 9/20/2014.
 */
public class EntityAsteroid extends Entity {
    private int x;
    private int y;
    private int speed;
    public EntityAsteroid(int x, int y, int speed) {
        super("Asteroid", EntityType.ASTEROID, new SpriteSheet(ImageLoader.ASTEROID, 2, 2, 128, 128), new Point(x, y));
        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    @Override
    public void tick() {
        this.adjustImageAccordingly();
        this.setPosition(x-=speed, (int) this.getPosition().getY());
    }

    private Image currentImage = this.getSheet().getImages()[0];
    private int index = 0;
    public void adjustImageAccordingly(){
        if(index == 3){
            index = 0;
            currentImage = this.getSheet().getImages()[0];
        }else{
            index++;
            currentImage = this.getSheet().getImages()[index];
        }
    }
    @Override
    public Image getImage() {
        return ImageLoader.ASTEROID;
    }

    public Image getCurrentImage(){
        return currentImage;
    }
    public int getSpeed(){
        return speed;
    }
}
