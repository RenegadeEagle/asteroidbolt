package me.renegadeeagle.co.projects.asteroidrunner.entity;

/**
 * Created by Ryan on 9/20/2014.
 */
public enum EntityType {
    ASTEROID,
    SHIP
}
